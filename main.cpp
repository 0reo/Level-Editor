#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cmath>
#include<vector>
#include <string.h>

using namespace std;

GLUquadric *quadric = gluNewQuadric();
int gridRows = 11;//depth
int gridCols = 11;//width
float size = 3.0;
GLint viewport[4];
GLdouble modelview[16];
GLdouble projection[16];
GLdouble winX, winY, winZ = 0.0;//window position for mouse
GLdouble posX, posY, posZ, nX, nY, nZ, fX, fY, fZ, aX, aY, aZ, bX, bY, bZ, cX, cY, cZ, NX, NY, NZ, NV, norm, D;// Hold The Final Values
GLdouble pX, pY, pZ, tX, tY, tZ;
bool LMB, RMB, debug, start;
int width = 800;
int height = 600;
GLubyte* myImg;
string filename;

//double eyes[3]={0.0, 0.1, 0.1};
double eyes[3]={0.0, 50.0, 100.0};
float ang = 0.0;
//double y1[100][100][3];

typedef struct _POINT
{
    double x;//values
    double y;
    double z;

    int posX;//positions
    int posY;

    _POINT* _X;
    _POINT* _Y;
}Point;

vector <vector <Point> > points;
Point selected;



void save(string name)
{
    char* file = new char[name.length() + 1];
    strcpy(file, name.c_str());

    ofstream myfile (file, ios::out);
    myfile << gridRows << "\n" << gridCols << "\n" << size << "\n";
    for (int i = 0; i < points.size(); i++)
    {
        for (int k = 0; k < points[i].size(); k++)
        {
            myfile << points.at(i).at(k).x << "\n";
            myfile << points.at(i).at(k).y << "\n";
            myfile << points.at(i).at(k).z << "\n";
        }
    }

    myfile.close();
    cout << "saved" << endl;
}


void loadFile(string name)
{

    char* file = new char[name.length() + 1];
    strcpy(file, name.c_str());

    ifstream data;
    string result;
cout << file << endl;
    data.open(file);
    if (data.is_open())
    {
        getline(data, result);
        gridRows = atoi(result.c_str());

        getline(data, result);
        gridCols = atoi(result.c_str());

        getline(data, result);
        size = strtod(result.c_str(), NULL);



        points.resize(gridRows);//depth


        for (int i = 0; i < points.size(); i++)
        {
            points[i] = ( vector<Point>() );
            points[i].resize(gridCols);//width

            for (int k = 0; k < points[i].size(); k++)
            {

                for (int j = 0; j < 3; j++)
                {
                    getline(data, result);
                    if (j == 0)
                        points.at(i).at(k).x = strtod(result.c_str(), NULL);
                    if (j == 1)
                        points.at(i).at(k).y = strtod(result.c_str(), NULL);
                    if (j == 2)
                        points.at(i).at(k).z = strtod(result.c_str(), NULL);
                }
            }
        }

        cout << "data should be loaded" << endl;
    }
    else
    {
        cout << "could not open" << endl;
    }
    data.close();

}



/* LoadPPM -- loads the specified ppm file, and returns the image data as a GLubyte
 *  (unsigned byte) array. Also returns the width and height of the image, and the
 *  maximum colour value by way of arguments
 *  usage: GLubyte myImg = LoadPPM("myImg.ppm", &width, &height, &max);
 */
GLubyte* LoadPPM(char* file, int* width, int* height, int* max)
{

    GLubyte* img;
    FILE *fd;
    int n, m;
    int  k, nm;
    char c;
    int i;
    char b[100];
    float s;
    int red, green, blue;

    fd = fopen(file, "r");
    fscanf(fd,"%[^\n]",b);

    if (b[0]!='P'|| b[1] != '3')
    {
        printf("%s is not a PPM file!\n",file);
        exit(0);
    }
    printf("%s is a PPM file\n", file);

    fscanf(fd, "%c",&c);
    while (c == '#')
    {
        cout << "yes" << endl;
        fscanf(fd, "%[^\n]", b);
        printf("%s\n",b);
        fscanf(fd, "%c",&c);
    }
    ungetc(c,fd);
    fscanf(fd, "%d %d %d", &n, &m, &k);

    printf("%d rows  %d columns  max value= %d\n",n,m,k);

    nm = n*m;

    img = (GLubyte*) malloc(3*sizeof(GLuint)*nm);


    s=255.0/k;


    for (i=0;i<nm;i++)
    {
        fscanf(fd,"%d %d %d",&red, &green, &blue );
        img[3*nm-3*i-3]=red*s;
        img[3*nm-3*i-2]=green*s;
        img[3*nm-3*i-1]=blue*s;
    }

    *width = n;
    *height = m;
    *max = k;

    return img;
}


void lights()
{
    float light_pos[] = {eyes[0], eyes[1], eyes[2], 1.0};
    float amb[] = {0.0, 0.0, 0.0, 1.0};
    float spc[] = {1.0, 1.0, 1.0, 1.0};
    float dif[] = {1.0, 1.0, 1.0, 1.0};

    glLightfv(GL_LIGHT1, GL_AMBIENT, amb);				// Setup The Ambient Light
    //glLightfv(GL_LIGHT1, GL_SPECULAR, spc);				// Setup The Ambient Light
    //glLightfv(GL_LIGHT1, GL_DIFFUSE, dif);				// Setup The Ambient Light
    glLightfv(GL_LIGHT0, GL_POSITION, light_pos);

}

void setUpGrid(int width, int depth, int spacing)
{
    //points.clear();
    points.resize(depth);


    for (int i = 0; i < points.size(); i++)
    {
        points[i] = ( vector<Point>() );
        points[i].resize(width);

        for (int k = 0; k < points[i].size(); k++)
        {
            points.at(i).at(k).x = spacing*i-((depth*spacing)/2.0); //point Z = new row;
            if (start)
                points.at(i).at(k).y = 0.0;
            points.at(i).at(k).z = spacing*k-((width*spacing)/2.0);

        }
    }
}

void drawGrid()
{
    Point *currentX, *currentY;
    for (int i = 0; i < gridRows; i++)//column loop, aka depth
    {
        glBegin(GL_TRIANGLE_STRIP);
        for (int k = 0; k < gridCols; k++)//width
        {
            glTexCoord2f(points.at(i).at(k).x/(gridRows*size), points.at(i).at(k).z/(gridCols*size));
            glVertex3f  (points.at(i).at(k).x, points.at(i).at(k).y, points.at(i).at(k).z);


            if (i +1 != gridRows)
            {
                glTexCoord2f(points.at(i+1).at(k).x/(gridRows*size),          points.at(i+1).at(k).z/(gridCols*size));
                glVertex3f  (points.at(i+1).at(k).x, points.at(i+1).at(k).y, points.at(i+1).at(k).z);
            }
        }
        glEnd();
    }
}

void changeWidth(int i)
{
    gridCols += i;
    if (gridCols > 2)
        setUpGrid(gridCols, gridRows, size);
}

void changedepth(int i)
{

    gridRows += i;
    if (gridRows > 2)
        setUpGrid(gridCols, gridRows, size);

}

void changeSpacing(int i)
{
    size += i;
    if (size > 0)
        setUpGrid(gridRows, gridCols, size);
}

void init(void)
{
    debug = false;
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);
    glEnable(GL_COLOR_MATERIAL);

    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(-width/2.0, width/2.0, -height/2.0, height/2.0,-600, 600);
    lights();

    float m_amb[] = {0.33, 0.22, 0.03, 1.0};
    float m_dif[] = {0.78, 0.57, 0.11, 1.0};
    float m_spec[] = {0.1, 0.1, 0.1, 1.0};
    float shiny = 0.0;


    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, m_amb);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, m_dif);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, m_spec);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shiny);


    glMatrixMode(GL_MODELVIEW);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_POINT_SMOOTH);

    glGetIntegerv(GL_VIEWPORT, viewport);
    glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
    glGetDoublev(GL_PROJECTION_MATRIX, projection);

    start = true;
    setUpGrid(gridCols, gridRows, size);
    int h = 256;
    int max = 255;

    glEnable(GL_TEXTURE_2D);
    myImg = LoadPPM("./textures/marble.ppm", &h, &h, &max);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 256, 256, 0, GL_RGB, GL_UNSIGNED_BYTE, myImg);


    glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_REPEAT );
    glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    glPolygonMode(GL_FRONT, GL_LINE);
    glPolygonMode(GL_BACK, GL_LINE);

    cout    << "Left Mouse - Raise points" << endl
    << "Right Mouse - Lower points" << endl
    << "Arrow Keys - Move Camera" << endl
    << "Home Key - Reset Camera" << endl
    << "Esc Key - Quit" << endl
    << "T Key - Change Texture" <<  endl
    << "F1 - help" <<  endl
    << "F2 - View Texture" <<  endl
    << "F3 - View as Grid" <<  endl
    << "F4 - Increase Depth" <<  endl
    << "F5 - Decrease Depth" <<  endl
    << "F6 - Increase Width" <<  endl
    << "F7 - Decrease Width" <<  endl
    << "F8 - Increase Cell Size" <<  endl
    << "F9 - Decrease Cell Size" <<  endl
    << "F10 - Save" <<  endl
    << "F11 - Load File" <<  endl
    << "F12 - Debug" <<  endl;

}

static void resize(int width, int height)
{
    const float ar = (float) width / (float) height;
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-ar, ar, -1.0, 1.0, 2.0, 600.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

static void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clear The Screen And The Depth Buffer
    glLoadIdentity();
    gluLookAt(eyes[0], eyes[1], eyes[2],//eyes
              0.0, 0.0, 0.0, //looking at
              0.0, 1.0, 0.0);//up direction
    glColor3d(0.66,0.33,0.0);
    drawGrid();
    glutSwapBuffers();
}

static void idle(void)
{
    if ((LMB) && (points[int(selected.x)][int(selected.y)].y <= 10.0))
        points[int(selected.x)][int(selected.y)].y+=0.05;
    if ((RMB) && (points[int(selected.x)][int(selected.y)].y >= -10.0))
        points[int(selected.x)][int(selected.y)].y-=0.05;
    glutPostRedisplay();
}


void spcFunc(int key, int x, int y)
{
    switch (key)
    {
    case GLUT_KEY_UP:
        eyes[2]+=1.5;
        lights();
        break;

    case GLUT_KEY_DOWN:
        eyes[2]-=1.5;
        lights();
        break;

    case GLUT_KEY_LEFT:
        eyes[0]-=1.5;
        lights();
        break;

    case GLUT_KEY_RIGHT:
        eyes[0]+=1.5;
        lights();
        break;

    case GLUT_KEY_HOME:
        eyes[0] = 0.0;
        eyes[1] = 50.0;
        eyes[2] = 100.0;
        //lights();
        break;

    case GLUT_KEY_F1:
        cout    << "Left Mouse - Raise points" << endl
        << "Right Mouse - Lower points" << endl
        << "Arrow Keys - Move Camera" << endl
        << "Home Key - Reset Camera" << endl
        << "Esc Key - Quit" << endl
        << "F1 - help" <<  endl
        << "F2 - View Texture" <<  endl
        << "F3 - View as Grid" <<  endl
        << "F4 - Increase Depth" <<  endl
        << "F5 - Decrease Depth" <<  endl
        << "F6 - Increase Width" <<  endl
        << "F7 - Decrease Width" <<  endl
        << "F8 - Increase Cell Size" <<  endl
        << "F9 - Decrease Cell Size" <<  endl
        << "F10 - Save" <<  endl
        << "F11 - Load File" <<  endl
        << "F12 - Debug" <<  endl;
        break;

    case GLUT_KEY_F2:
        glPolygonMode(GL_FRONT, GL_FILL);
        glPolygonMode(GL_BACK, GL_FILL);
        break;

    case GLUT_KEY_F3:
        glPolygonMode(GL_FRONT, GL_LINE);
        glPolygonMode(GL_BACK, GL_LINE);
        break;

    case GLUT_KEY_F4:
        changedepth(1);
        break;

    case GLUT_KEY_F5:
        changedepth(-1);
        break;
    case GLUT_KEY_F6:
        changeWidth(1);
        break;

    case GLUT_KEY_F7:
        changeWidth(-1);
        break;

    case GLUT_KEY_F8:
        changeSpacing(1);
        break;

    case GLUT_KEY_F9:
        changeSpacing(-1);
        break;

    case GLUT_KEY_F10:
        cout << "Enter name of file" << endl;
        while (!(cin >> filename))
        {
            cin.clear() ;
            cin.ignore(256,'\n') ;
            cout << "Bad Input - Try again" << endl;
        }
        save("./saved/"+filename);
        break;

    case GLUT_KEY_F11:
        cout << "Enter name of file to load" << endl;
        while (!(cin >> filename))
        {
            cin.clear() ;
            cin.ignore(256,'\n') ;
            cout << "Bad Input - Try again" << endl;
        }
        loadFile("./saved/"+filename);
        break;

    case GLUT_KEY_F12:
        debug = !debug;
        break;
    }
    glutPostRedisplay();
}

void keyFunc(unsigned char key, int x, int y)
{
    if (key == 27)
        exit(0);
    if (key == 't' || key == 'T')
        cout << "changing textures is not yet implimented" << endl;

}
void manipulate(int x, int y)
{
    winX = double(x);
    winY = double(y);
    winY = (float)viewport[3] - winY;
    glReadPixels(winX, winY, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ);

    glGetIntegerv(GL_VIEWPORT, viewport);
    glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
    glGetDoublev(GL_PROJECTION_MATRIX, projection);

    gluUnProject( winX, winY, 0.0, modelview, projection, viewport, &nX, &nY, &nZ);
    gluUnProject( winX, winY, 1.0, modelview, projection, viewport, &fX, &fY, &fZ);
    gluUnProject( winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ); //where mouse is, aka S

    cX = fX - nX;//direction of ray, aka V
    cY = fY - nY;//direction of ray
    cZ = fZ - nZ;//direction of ray

    aX = points[1][2].x - points[0][0].x;
    aY = points[1][2].y - points[0][0].y;
    aZ = points[1][2].z - points[0][0].z;

    bX = points[2][2].x - points[1][2].x;
    bY = points[2][2].y - points[1][2].y;
    bZ = points[2][2].z - points[1][2].z;

    NX = (aY * bZ) - (aZ * bY);
    NY = (aZ * bX) - (aX * bZ);
    NZ = (aX * bY) - (aY * bX);


    norm = sqrt((NX*NX)+(NY*NY)+(NZ*NZ));
    NX = NX/norm;
    NY = NY/norm;
    NZ = NZ/norm;


    D = ((-1.0 * NX) * points[2][1].x )+((-1.0 * NY) * points[2][1].y )+((-1.0 * NZ) * points[2][1].z );
    double d = ((NX * posX )+(NY * posY )+(NZ * posZ )) + D;

    NV = ((NX*cX)+(NY*cY)+(NZ*cZ));

    tX = (( (NX*posX) + (NY*posY) + (NZ*posZ) + D  ) * -1.0) / NV;

    pX = posX+(tX*cX);//intersection x
    pY = posY+(tX*cY);//intersection y
    pZ = posZ+(tX*cZ);//intersection z

    if (debug)
    {
        cout << pX << " " << pY << " " << pZ << " " << "intersect" <<endl;
        cout << D+posY << endl;
        cout << tX << " T" <<endl;
    }

    if (LMB || RMB)
    {
        for (int i = 0; i < gridRows; i++)
        {
            for (int j = 0; j < gridCols; j++)
            {

                if (pX >= points[i][j].x-(size/2.0) && pX <= points[i][j].x+(size/2.0)
                        && pY >= points[i][j].y-(size/2.0) && pY <= points[i][j].y+(size/2.0)
                        && pZ >= points[i][j].z-(size/2.0) && pZ <= points[i][j].z+(size/2.0))
                {
                    selected.x = i;
                    selected.y = j;
                }
            }
        }
    }
}

void mouse(int btn, int state, int x,int y)
{
    if (btn == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
        LMB = true;
    else
        LMB = false;
    if (btn == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
        RMB = true;
    else
        RMB = false;
    manipulate(x,y);
}


void motion(int x, int y)
{

    manipulate(x,y);
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitWindowSize(width,height);
    glutInitWindowPosition(10,10);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
    glutCreateWindow("Terrain Editor, beta");
    glutReshapeFunc(resize);

    glutDisplayFunc(display);
    glutIdleFunc(idle);
    glutSpecialFunc(spcFunc);
    glutKeyboardFunc(keyFunc);
    glutMouseFunc(mouse);
    glutMotionFunc(motion);

    init();
    glutMainLoop();

    return EXIT_SUCCESS;
}
