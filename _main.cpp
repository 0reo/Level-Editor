#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>
#include <cstdlib>
#include <iostream>
#include <cmath>
#include <fstream>


using namespace std;
GLUquadric *quadric = gluNewQuadric();
int gridRows = 35;
int gridCols = 20;
int size = 10;
GLint viewport[4];
GLdouble modelview[16];
GLdouble projection[16];
GLdouble winX, winY, winZ = 0.0;//window position for mouse
GLdouble posX, posY, posZ, nX, nY, nZ, fX, fY, fZ, aX, aY, aZ, bX, bY, bZ, cX, cY, cZ, NX, NY, NZ, NV, norm, D;// Hold The Final Values
GLdouble pX, pY, pZ, tX, tY, tZ;
bool LMB, RMB;
int width = 800;
int height = 600;


//double eyes[3]={0.0, 0.1, 0.1};
double eyes[3]={10.0, 5.0, 10.0};
float ang = 0.0;
//double y1[100][100][3];

typedef struct _POINT
{
    double x;
    double y;
    double z;
}Point;

Point points[500][500];
Point selected;

void init(void)
{

    cout << "use arrow keys to adjust camera.  press home key to put into editing position.  hold left mouse down to raise a point."
    << "hold right mouse down to lower a point.  you can click and drag the mouse to make a path" << endl;
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(-width/2, width/2, -height/2, height/2,-600, 600);
    //gluPerspective(60.0f, 1.0f, -400.0f, 400.0f);  //have option to turn on and off


    glMatrixMode(GL_MODELVIEW);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_POINT_SMOOTH);

    glClearColor(0,0,0,0);

    glGetIntegerv(GL_VIEWPORT, viewport);
    glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
    glGetDoublev(GL_PROJECTION_MATRIX, projection);

    points[0][0].x = 0.0;
    points[0][0].y = 0.0;
    points[0][0].z = 0.0;
    for (int i = 0; i < gridRows; i++)
    {

        for (int k = 0; k < gridCols; k++)
        {
            if (k != 0)
            {
                points[i][k].x = points[i][k-1].x +(size);
            }
            else
                points[i][k].x = -(gridCols*size)/2;
            points[i][k].y = 0.0;
            points[i][k].z = size*i-((gridRows*size)/2);
        }
    }
}



static void display(void)
{

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clear The Screen And The Depth Buffer
    //glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(eyes[0], eyes[1], eyes[2],//eyes
              0.0, 0.0, 0.0, //looking at
              0.0, 1.0, 0.0);//up direction

    //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glColor3d(0,1,0);

    double x= (gridCols*size)/2;
    double y = 0.0;
    double z = (gridRows*size)/2;

    glBegin(GL_LINE_STRIP);
    glPushMatrix();
    for (int i = 0; i < gridRows; i++)
    {
        for (int j = 0; j < gridCols; j++)
        {
            glVertex3f(points[i][j].x, points[i][j].y, points[i][j].z);
        }

        for (int k = gridCols-1; k > 0; k--)
        {
            if (i+1 != gridRows)
            {
                glVertex3f(points[i+1][k].x, points[i+1][k].y, points[i+1][k].z);
                glVertex3f(points[i][k-1].x,points[i][k-1].y, points[i][k-1].z);
            }
        }
        if (i+1 != gridRows)
            glVertex3f(points[i+1][0].x,points[i+1][0].y, points[i+1][0].z);

    }



    //glRotatef(ang, 1.0, 1.0, 1.0);
    glPopMatrix();
    glEnd();
    glutSwapBuffers();


}

static void idle(void)
{
    //glutSwapBuffers();
    //glPopMatrix();
    //cout << posX << " " << posY << " " << posZ << endl;
    //cout << selected.x << " " << selected.y << " " << selected.z << " " << endl;
    if (LMB)
        points[int(selected.x)][int(selected.y)].y+=1.5;
    if (RMB)
        points[int(selected.x)][int(selected.y)].y-=1.5;
    glutPostRedisplay();

}


void spcFunc(int key, int x, int y)
{

    switch (key)
    {
    case GLUT_KEY_UP:
        eyes[1]+=1.5;
        break;

    case GLUT_KEY_DOWN:
        eyes[1]-=1.5;
        break;

    case GLUT_KEY_LEFT:
        eyes[0]-=1.5;
        break;

    case GLUT_KEY_RIGHT:
        eyes[0]+=1.5;
        break;

    case GLUT_KEY_HOME:
        eyes[0] = 0.0;
        eyes[1] = -90.0;
        break;
    }
    glutPostRedisplay();
}


void manipulate(int x, int y)
{
    winX = double(x);
    winY = double(y);
    winY = (float)viewport[3] - winY;
//    glReadPixels(winX, winY, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ);
//
//    glGetIntegerv(GL_VIEWPORT, viewport);
//    glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
//    glGetDoublev(GL_PROJECTION_MATRIX, projection);
//
//    gluUnProject( winX, winY, 0.0, modelview, projection, viewport, &nX, &nY, &nZ);
//    gluUnProject( winX, winY, 1.0, modelview, projection, viewport, &fX, &fY, &fZ);
//    gluUnProject( winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ); //where mouse is, aka S
    posX = winX-width/2;
    posY = winY-height/2;

    cX = fX - nX;//direction of ray, aka V
    cY = fY - nY;//direction of ray
    cZ = fZ - nZ;//direction of ray
//cols = width = X
//rows = depth = Z

    aX = points[1][2].x - points[0][0].x;
    aY = points[1][2].y - points[0][0].y;
    aZ = points[1][2].z - points[0][0].z;

    bX = points[2][2].x - points[1][2].x;
    bY = points[2][2].y - points[1][2].y;
    bZ = points[2][2].z - points[1][2].z;

    NX = (aY * bZ) - (aZ * bY);
    NY = (aZ * bX) - (aX * bZ);
    NZ = (aX * bY) - (aY * bX);


    norm = sqrt((NX*NX)+(NY*NY)+(NZ*NZ));
    NX = NX/norm;
    NY = NY/norm;
    NZ = NZ/norm;

    NX = 0.0;
    NY = -1.0;
    NZ = 0.0;

    D = ((-1.0 * NX) * points[2][1].x )+((-1.0 * NY) * points[2][1].y )+((-1.0 * NZ) * points[2][1].z );

    NV = ((NX+cX)+(NY+cY)+(NZ+cZ));

    tX = ( (NX*posX) + (NY*posY) + (NZ*posZ) + D  ) * -1.0 / NV;


    pX = posX+(tX*cX);
    pY = posY+(tX*cY);
    pZ = posZ+(tX*cZ);

//    cout << posX << " " << posY << " " << posZ << " " << "mouse position" <<endl;
////    cout << cX << " " << cY << " " << cZ << " " << "mouse direction" <<endl;
//    cout << pX << " " << pY << " " << pZ << " " << "intersect" <<endl;
//    cout << NX << " " << NY << " " << NZ << " normal" <<endl;
//    cout << tX << " T" <<endl;

//p = posX+t*cX

    if (LMB || RMB)
    {
        for (int i = 0; i < gridRows; i++)
        {
            for (int j = 0; j < gridCols; j++)
            {

                if (posX >= points[i][j].x-(size/2) && posX <= points[i][j].x+(size/2)
                        && posY >= points[i][j].z-(size/2) && posY <= points[i][j].z+(size/2))
                {
                    selected.x = i;
                    selected.y = j;
                }
                //else
                //cout << points[i][j].x << " " << points[i][j].z << " point raised"<< endl;
                //cout << points[i][j].z << "yo" <<endl;
            }
        }
    }




}

void mouse(int btn, int state, int x,int y)
{
    if (btn == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
        LMB = true;
    else
        LMB = false;
    if (btn == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
        RMB = true;
    else
        RMB = false;
    manipulate(x,y);
}


void motion(int x, int y)
{

    manipulate(x,y);
}
/* Program entry point */

//void loadFile(char* file)
//{
//
//    ifstream data;
//    data.open(file);
//    int lines = 0;
//    //char result[8];
//    string result;
//    int count = 0;
//
//    char output[30000][21];
//    char ran[30000][21];
//    char training;
//    char validation;
//    //char[] myFile[1000];
//
//
//
//    //output[0] = grid rows
//    //output[1] = grid cols
//
//    for (int i = 0; i < int(output[0]); i++)
//    {
//
//        for (int k = 0; k < int(output[1]); k++)
//        {
//
//            if (data.is_open())
//            {
//                while (!data.eof())
//                {
//                    double(data) >> points[i][k].x;
//                    lines++;
//                    double(data) >> points[i][k].y;
//                    lines++;
//                    double(data) >> points[i][k].z;
//                    lines++;
//                }
//            }
//        }
//    }
//
//    data.close();
//
//}

void save(char* name)
{




    ofstream myfile;
    myfile.open (name);
    myfile << gridRows << "\n" << gridCols << "\n";
    for (int i = 0; i < gridRows; i++)
    {

        for (int k = 0; k < gridCols; k++)
        {
            myfile << points[i][k].x << "\n";
            myfile << points[i][k].y << "\n";
            myfile << points[i][k].z << "\n";
        }
    }

    myfile.close();

}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitWindowSize(width,height);
    glutInitWindowPosition(10,10);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
    cout << "Enter no. of grid rows" << endl;
    cin >> gridRows;
    cout << "Enter no. of grid cols" << endl;
    cin >> gridCols;
    glutCreateWindow("Terrain Editor, beta");
    glutDisplayFunc(display);
    glutIdleFunc(idle);
    glutSpecialFunc(spcFunc);
    glutMouseFunc(mouse);
    glutMotionFunc(motion);
    init();
    glutMainLoop();

    return EXIT_SUCCESS;
}
